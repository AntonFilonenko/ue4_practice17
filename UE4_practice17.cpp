﻿#include <iostream>

class Vector
{
private:
    double x = 0, y = 0, z = 0;
public:
    void Show() 
    {
        std::cout << "x=" << x << " y=" << y << " z=" << z << "\n";
    }

    Vector(double NewX, double NewY, double NewZ) : x(NewX), y(NewY), z(NewZ)
    {}

    void SetX(double NewX)
    {
        x = NewX;
    }
    void SetY(double NewY)
    {
        y = NewY;
    }
    void SetZ(double NewZ)
    {
        z = NewZ;
    }
    void SetVector(double NewX, double NewY, double NewZ)
    {
        x = NewX;
        y = NewY;
        z = NewZ;
    }

    double GetX()
    {
        return x;
    }
    double GetY()
    {
        return y;
    }
    double GetZ()
    {
        return z;
    }

    double VectorModule()
    {
        return sqrt(pow(x, 2) + pow(y, 2) + pow(z, 3));
    }
};

int main()
{
    Vector MyVector(3.0, 1.5, 2.1);
    MyVector.Show();
    std::cout << MyVector.VectorModule() << "\n";
    MyVector.SetX(0.5);
    MyVector.Show();
    std::cout << MyVector.VectorModule() << "\n";
    MyVector.SetVector(1, 1, 1);
    MyVector.Show();
    std::cout << MyVector.VectorModule() << "\n";
}
